module.exports = (flags) => {
	if (flags.debug) {
		require("debug").enable("msr:*");
	}

	// Imports.
	const getPackagePaths = require("../lib/getPackagePaths");
	const semanticReleaseMulti = require("../lib/semanticReleaseMulti");
	const multisemrelPkgJson = require("../package.json");
	const semrelPkgJson = require("semantic-release/package.json");

	// Get directory.
	const cwd = process.cwd();

	// Catch errors.
	try {
		console.log(`semantic-release-multi version: ${multisemrelPkgJson.version}`);
		console.log(`semantic-release version: ${semrelPkgJson.version}`);
		console.log(`flags: ${JSON.stringify(flags, null, 2)}`);

		// Get list of package.json paths according to workspaces.
		const paths = getPackagePaths(cwd, flags.ignorePackages);
		console.log("package paths", paths);

		// Do multirelease (log out any errors).
		semanticReleaseMulti(paths, {}, { cwd }, flags).then(
			() => {
				// Success.
				process.exit(0);
			},
			(error) => {
				// Log out errors.
				console.error(`[semantic-release-multi]:`, error);
				process.exit(1);
			}
		);
	} catch (error) {
		// Log out errors.
		console.error(`[semantic-release-multi]:`, error);
		process.exit(1);
	}
};
