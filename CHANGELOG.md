# 1.0.0 (2023-01-09)


### Features

* initial release ([a4f2aa4](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-multi/commit/a4f2aa45f1073060248ba3c677dde25379320798))
* initial release ([fc63174](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-multi/commit/fc63174b08c500f1faac4e2cf6add6e1bde81eb0))
